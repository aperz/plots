
metrs = pd.DataFrame({  'method1': {'prec': 0.98, 'rec': 0.89},
                        'method2': {'prec': 0.93, 'rec': 0.72}})


# Barplot for comparing performance metrics
def compare_performance_metrics(data=metrs)
    n_groups = 2
    data_regex = data['method1'].tolist()
    data_abie = data['method2'].tolist()

#print(data_regex)
#print(data_abie)

    # create plot
    fig, ax = plt.subplots()
    index = np.arange(n_groups)
    bar_width = 0.35
    opacity = 0.8

    rects1 = plt.barh(index, data_regex, bar_width,
        alpha=opacity,
        color='b',
        label='Method 1')

    rects2 = plt.barh(index + bar_width, data_abie, bar_width,
        alpha=opacity,
        color='g',
        label='Method 2')

    plt.ylabel('method')
    plt.xlabel('score')
    #plt.title('')
    plt.yticks(index + bar_width/2, ('precision', 'recall'))
    plt.legend(loc=3)

    for i, v in zip(
        [0-bar_width/2,0+bar_width/2,1-bar_width/2,1+bar_width/2],
        [data_regex[0],data_abie[0],data_regex[1],data_abie[1]]
        ):
            ax.text(v-0.16,i+0.06, str(v), color='white', fontweight='bold', fontsize="16")

    plt.tight_layout()
    plt.show()


# barplot with stacked / overlapping bars
def stacked_barplot(data)
    n_groups = 3
    data_main = X[:3]
    data_overlay = X[3:6]
    data_overlay2 = X[6:9]

    fig, ax = plt.subplots()
    index = np.arange(n_groups)
    bar_width = 0.35
    opacity = 1

    rects1 = plt.barh(index, data_main, bar_width,
        alpha=opacity,
        color='b',
        label='extracted items')

    # these will overlap rects 1 (from data_main) at the beginning
    rects2 = plt.barh(index, data_overlay, bar_width,
        alpha=opacity,
        color='red',
        label='erroneous items')

    # these will overlap rects 1 (from data_main) at the ends
    rects3 = plt.barh(index, data_overlay2, bar_width,
        alpha=opacity,
        color='darkgrey',
        label='estimated misextracted items',
        left=[i-j for i,j in zip(data_main, data_overlay2)])

    plt.ylabel('item type')
    plt.xlabel('%')
    #plt.title('')
    plt.yticks(index, ("URL", "ID", "ES"))
    #plt.legend(loc=3)

    #https://stackoverflow.com/questions/4700614/how-to-put-the-legend-out-of-the-plot
    # Shrink current axis's height by 10% on the bottom
    box = ax.get_position()
    ax.set_position([   box.x0, box.y0 + box.height * 0.1,
                        box.width, box.height * 0.25])

    # Put a legend below current axis
    ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.35),
            #fancybox=True,
            #shadow=True, ncol=1)
            )
    plt.tight_layout()
    plt.show()
