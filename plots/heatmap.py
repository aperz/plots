#!/usr/bin/env python3


import pandas as pd
import numpy as np
import argparse
import os
from os.path import splitext
import matplotlib.pyplot as plt
from matplotlib.colors import rgb2hex, colorConverter
import fastcluster
import seaborn as sns


def representation_with_levels(D, use_index_level=[1], axis=0, **kwargs):
    '''
    Input:  ?? A distance matrix with a Multilevel index. Make sure D.index == D.columns
    Output: A plot; samples coloured by project and treatment on the margins.
    A brief description of the projects most close to the "focus sample".
    # TODO: axis arg
    '''

    #TODO: axis arg
    #TODO: auto determine type: diverging, >0 scale, boolean
    col_color_list = []
    for il in use_index_level:
        used_level2 = D.columns.get_level_values(il).drop_duplicates().tolist()
        level2_pal = sns.color_palette( "Set2", n_colors=len(used_level2))
        level2_lut = dict(zip(used_level2, level2_pal))

        # Convert the palette to vectors that will be drawn on the side of the matrix
        level2 = D.columns.get_level_values(il)
        level2_colors = pd.Series(level2, index=D.columns).map(level2_lut)
        col_color_list.append(level2_colors)

    # Create a custom colormap for the heatmap values
    #if len(set([i for i in itertools.chain.from_iterable(D.values)])) == 2:
    #cmap = sns.diverging_palette(h_neg=210, h_pos=350, s=90, l=30, as_cmap=True)

    # Draw the full plot
    p = sns.clustermap(D,
        col_colors=pd.concat(col_color_list, axis=1),
        #row_cluster = False, # cluster by predefined
        linewidths=.5,
        #cmap=cmap,
        #vmin=0, vmax=1,
        **kwargs,
        )

    plt.setp(p.ax_heatmap.yaxis.get_majorticklabels(), rotation=0)
    return p






