import seaborn as sns
import pylatexenc

import mnemonic.statistics

def fix_index(ix):
    # for LaTeX output
    return [pylatexenc.latexencode.utf8tolatex(k) for k in ix]

def clustermap(X, color_xticks=None, color_yticks=None, transpose=False, cmap="RdBu_r", **kwargs):
    """
    color_xticks, color_yticks: dict
        color -> list<label>
        e.g. {"red": ["C1","C3","C5"]}
    """
    X = mnemonic.statistics.impute(X.copy())
    X.index = fix_index(X.index)
    X.columns = fix_index(X.columns)
    if transpose:
        X = X.T
    cg = sns.clustermap(X, cmap=cmap, **kwargs)

    # Set colors
    def color_ticks(axis, M):
        if M is None:
            return
        ix = [t.get_text() for t in axis.get_ticklabels()]
        for color, labels in M.items():
            for label in labels:
                # FIXME:
                try:
                    axis.get_ticklabels()[ix.index(label)].set_color(color)
                except:
                    pass
    color_ticks(cg.ax_heatmap.xaxis, color_xticks)
    color_ticks(cg.ax_heatmap.yaxis, color_yticks)
    return cg


def cool_barplot_with_density(pandas_series, fontsize=20, figsize=(10,6)):
"""
uses whatsmydata.distribution_fitting
"""
    fig = plt.figure(figsize=figsize)

    # Make the labels a little more visible
    ax = fig.add_subplot(111)
    #fig.subplots_adjust(left=0.45)
    fig.tight_layout()
    #fig.subplots_adjust(top=-0.1)

    pos = [i for i in range(len(pandas_series))]
    bars = plt.bar(pos, pandas_series.values, color=palette['blue'], width=1, linewidth=0.4)
    #for bar in bars:
    #    plt.gca().text(bar.get_x()+bar.get_width()/3, #yloc,
    #                   max(pandas_series), #xloc
    #                   str(int(bar.get_height()))
    #                  ,
    #    ha='right', color=palette['black'], fontsize=fontsize)
    plt.xticks(pos, pandas_series.index, rotation='horizontal', fontsize=fontsize);
    ax.set_ylabel(pandas_series.name)
    ax.set_xlabel(pandas_series.index.name)
    ylim = ax.get_ylim()

    # Find best fit distribution
    best_fit_name, best_fit_params, _ = wmd.distribution_fitting.best_fit_distribution(data, 2000, ax)
    best_dist = getattr(scipy.stats, best_fit_name)
    # Make PDF
    pdf = wmd.distribution_fitting.make_pdf(best_dist, best_fit_params)
    pdf.plot(lw=2, label='PDF', legend=True)

    param_names = (best_dist.shapes + ', loc, scale').split(', ') if best_dist.shapes else ['loc', 'scale']
    param_str = ', '.join(['{}={:0.2f}'.format(k,v) for k,v in zip(param_names, best_fit_params)])
    dist_str = '{}({})'.format(best_fit_name, param_str)

    ax.set_title(u'Best fit distribution \n' + dist_str)
    #ax.set_xlabel(u'Temp. (°C)')
    #ax.set_ylabel('Frequency')


    ax.set_ylim(ylim)

    return fig
