# some libs are not necessary
from . import toolbox as tb
from . import objects as ob
import pandas as pd
import numpy as np
import ast
import re
import os

import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches



from pubqc.errch.classify import *

n_checked = 24740200 #FIXME this value from len(pmids) (statistic_match_porci)

my_palette = {
    'red'   : sns.color_palette('muted')[2],
    'black' : sns.light_palette('black')[-2],
    'blue'  : sns.color_palette('muted')[0],
    }


def diff_freq_plot(x, ylim=[0,100]):
    fontsize=15
    scale=5
    fig = plt.figure(num=None, figsize=(1*scale,1*scale), dpi=300, facecolor='w',
                     edgecolor='w')
    y = [i for i in range(len(x))]
    plt.scatter(y,x, alpha=0.5, s=15)
    plt.ylim(ylim)

    # remove all the ticks (both axes), and tick labels on the Y axis
    plt.tick_params(top='off', bottom='off', left='off', right='off',
                    labelleft='on', labelbottom='on')

    for spine in plt.gca().spines.values():
        spine.set_visible(False)



def pval_scatterplot(x,y, title = "", color="p-value",
                palette = my_palette, lims=None, **kwargs):
    #sns.set_style('whitegrid')
    fontsize=15
    scale=10
    #barHeight = 1
    #fig, ax = plt.subplots(figsize=(10,3))
    fig = plt.figure(num=None, figsize=(1*scale,1*scale), dpi=300, facecolor='w',
                     edgecolor='w')

    if color=="p-value":
        colors = [palette['red'] if i else palette['black'] for i in ((y < 0.05) & (x > 0.05)).tolist()]
    #colors = [palette['red'] if i else palette['black'] for i in (x < 0.05).tolist()]
    else:
        colors = [palette['black'] for i in x.tolist()]

    plt.scatter(x,y, c = colors, alpha = 0.5, s=15, **kwargs)

    # remove all the ticks (both axes), and tick labels on the Y axis
    plt.tick_params(top='off', bottom='off', left='off', right='off',
                    labelleft='on', labelbottom='on')

    # remove the frame of the chart
    for spine in plt.gca().spines.values():
        spine.set_visible(False)

    ax = fig.add_subplot(111)
    plt.axis('equal')
    if lims:
        plt.axis(lims)
    #plt.xlim((0,1))
    #plt.ylim((0,1))

    ax.set_title(title)

    ax.set_xlabel(x.name)
    ax.set_ylabel(y.name)

    #fig.subplots_adjust(bottom=0.20)

    #return fig

def stacked_barplot(data, colname, rows, label_colname=None, fontsize=15):
    plt.close()
    bar_width = 1
    scale=2
    #barHeight = 1
    #fig, ax = plt.subplots(figsize=(10,3))
    fig = plt.figure(num=None, figsize=(10*scale,1*scale), dpi=300, facecolor='w',
                     edgecolor='w')
    #axes = fig.add_subplot(1,1,1)
    #fig.subplots_adjust(left=0.35)

    #FIXME Set2 only has 5 colors
    pal = dict(zip(rows, [sns.color_palette("Set2")[r%5] for r in rows]))
    #print(pal)
    #x_pos = 0
    y_pos = bar_width*0.45
    x_pos_end =0
    x_pos_start =0
    count = 0
    patches = []
    total_len = data[colname].sum()

    for r in rows:
        x_len = int(data[colname].iloc[r])
        if label_colname:
            label = str(data[label_colname].iloc[r])
        else:
            label = ""
        x_pos_start = x_pos_end
        x_pos_end = x_pos_start+x_len
        #print(x_pos_start, x_pos_end, x_len)

        bar = plt.broken_barh([(x_pos_start, x_len)], (0,bar_width),
                facecolors=pal[r], edgecolor='white',
                             )
        if count%2:
            y_pos_ = y_pos*0.60
        else:
            y_pos_ = y_pos*1.40

        plt.gca().text(
            (x_pos_start+x_pos_end)/2, y_pos_,
            str(x_len)+" ("+str(round(x_len/total_len*100,2))+"%)",
            ha='center', color='k', fontsize=fontsize
        )
        #x_pos += int(data[colname][r])
        count+=1
        patches.append(mpatches.Patch(color=pal[r],
                                      label = "(" +str(x_len) + " hits) "+label))


    #plt.title('Title', fontsize=fontsize)
    #plt.x_label()
    #plt.yticks(pos, pandas_series.index, rotation='horizontal', fontsize=fontsize)
    plt.tick_params(
        axis='y',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        #left='off',      # ticks along the bottom edge are off
        #top='off',         # ticks along the top edge are off
        labelleft='off') # labels along the bottom edge are off
    for spine in plt.gca().spines.values():
        spine.set_visible(False)
    plt.axis('off')
    #ax.grid(False)

    for spine in plt.gca().spines.values():
        spine.set_visible(False)

    plt.legend(handles=patches, bbox_to_anchor=(0.04, 1.1, 1., .102), loc=4,
           ncol=1, mode="expand", borderaxespad=0., fontsize=fontsize)
    #fig.tight_layout()
    #plt.subplots_adjust(left=0, right=1, top=1, bottom=0)

def cool_barplot(pandas_series, fontsize=20, colors=None, add_percent=False, **kwargs):

    fig = plt.figure(num=None, dpi=300, facecolor='w', edgecolor='w', **kwargs)
    pos = [i for i in range(len(pandas_series))]
    #total_len = pandas_series.sum()

    if colors == None:
        bars = plt.barh(pos, pandas_series.values)
    else:
        bars = plt.barh(pos, pandas_series.values, color=colors)
    for bar in bars:
        if add_percent:
            txt = str(int(bar.get_width()))+" ("+str(round(bar.get_width()/add_percent*100, 2))+"%)"
        else:
            txt = str(int(bar.get_width()))
        plt.gca().text(max(pandas_series), bar.get_y()+bar.get_height()/3, txt,
            ha='right', color='k', fontsize=fontsize)
        plt.yticks(pos, pandas_series.index, rotation='horizontal', fontsize=fontsize);
    #return fig
    for spine in plt.gca().spines.values():
        spine.set_visible(False)

